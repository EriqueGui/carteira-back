const bcrypt = require('bcrypt')
const authHelper = require('../helper/auth.helper')
const repository = require('../repository/user.repository')
 
exports.login = async (loginData) => {

    let user = await repository.findByLogin(loginData, login)

    if(user == null){
        throw new Error("Dados inválidos para o login!")
    }  else{
        let isPassowrdValid = bcrypt.compareSync(loginData.password, user.password)

        if(isPassowrdValid){
            let token = authHelper.createToken(user)

            return{
                user: user.name,
                roles: user.roles,
                token: token
            }
        }else{
            throw new Error("Dados inválidos para o login!")
        }
    }

}