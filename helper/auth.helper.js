const jwt = require('jsonwebtoken')

const privateKey = '%&#5i573m4s=+/0'

exports.createToken = (user) => {

    let payLoad ={
        user: user.Name,
        roles: user.roles
    }

    let token = jwt.sign(payLoad, privateKey, {
        subject: user._id,
        expiresIn: 600
    })

}