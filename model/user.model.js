const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    login: String,
    password: String,
    entry: Number,
    exits: Number,
    saldo: Number,
    name: String,
    picture: File,
    creditCardCurrent: Number,
    creditCardlast: Number
})

module.exports = mongoose.model('User', userSchema)