const mongoose = require('mongoose')

const transactionSchema = mongoose.Schema({
    type: Number,
    value: Number,
    date: Date,
    description: String,
    category: String,
    fixed: Boolean,
    qtd: Number

})

module.exports = mongoose.model('Transaction', transactionSchema)